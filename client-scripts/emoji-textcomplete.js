! function() {
    var TEXTCOMPLETE_OPTIONS, defaultLists, defaultUsage, exports, orig_src, globElement;
    return TEXTCOMPLETE_OPTIONS = {
            className: "textcomplete textcomplete-emoji",
            zIndex: 12e3
        }, defaultLists = {
            chest: ["ambus_charge","ashrend","atziris_splendour","belly_of_the_beast","bramblejack","briskwrap","bronns_lithe","carcass_jack","cherrubims_maleficence","cloak_of_defiance","cloak_of_flame","daressos_defiance","deaths_oath","foxshade","greeds_embrace","hyrris_ire","icetomb","incandescent_heart","infernal_mantle","kaoms_heart","kingsguard","lightbane_raiment","lightning_coil","lioneyes_vision","queen_of_the_forest","shavronnes_wrappings","solaris_lorica","soul_mantle","tabula_rasa","the_covenant","the_rat_cage","the_restless_ward","thousand_ribbons","victarios_influence","vis_mortis","volls_protector","zahndethus_cassock"],
			rings: ["andvarius","bereks_grip","bereks_pass","bereks_respite","blackheart","bloodboil","brinerot_mark","call_of_the_brotherhood","death_rush","demigods_eye","doedres_damning","dream_fragments","emberwake","gifts_from_above","heartbound_loop","kaoms_sign","kikazaru","le_heup_of_all","loris_lantern","malachais_artifice","mings_heart","mokous_embrace","mutewind_seal","ngamahus_sign","perandus_signet","pyre","redblade_band","romiras_banquet","shavronnes_revelation","sibyls_lament","tasalios_sign","the_pariah","the_taming","thiefs_torment","timeclasp","valakos_sign","ventors_gamble","voideye"],
			amulets: ["araku_tiki","astramentis","atziris_foible","blood_of_corruption","bloodgrip","carnage_heart","daressos_salute","demigods_presence","eye_of_chayula","karui_ward","marylenes_fallacy","rashkaldors_patience","shapers_seed","sidhebreath","stone_of_lazhwar","talisman_of_the_victor","tear_of_purity","the_anvil","the_ignomon","ungils_harmony","victarios_acuity","volls_devotion","warped_timepiece"],
			belt: ["auxium","bated_breath","belt_of_the_deceiver","demigods_bounty","doryanis_invitation","dyadian_dawn","headhunter","immortal_flesh","maligaros_restraint","meginords_girdle","perandus_blazon","prismweave","soulthirst","sunblast","the_magnate","wurms_molt"],
			gloves: ["asenaths_gentle_touch","atziris_acuity","aurseize","demigods_touch","doedres_tenure","doryanis_fist","empires_grasp","facebreaker","flesh_and_spirit","hrimsorrow","lochtonial_caress","maligaros_virtuosity","meginords_vise","null_and_void","ondars_clasp","repentance","sadimas_touch","shackles_of_the_wretched","shadows_and_dust","slitherpinch","snakebite","southbound","surgebinders","thunderfist","vaal_caress","voidbringer","winds_of_change"],
			boots: ["alberons_warpath","atziris_step","bones_of_ullr","brinerot_whalers","darkray_vectors","deerstalker","demigods_stride","dusktoe","gangs_momentum","goldwyrm","kaoms_roots","lioneyes_paws","mutewind_whispersteps","nomics_storm","rainbowstride","redblade_tramplers","shavronnes_pace","sin_trek","steppan_eard","sundance","the_blood_dance","victarios_flight","wake_of_destruction","wanderlust","windscream","wondertrap"],
			helmets: ["abyssus","alphas_howl","asenaths_mark","black_sun_crest","chitus_apex","crown_of_eyes","crown_of_the_pale_king","crown_of_thorns","deidbell","demigods_triumph","devotos_devotion","doedres_scorn","ezomyte_peak","fairgraves_tricorne","geofris_crest","goldrim","heatshiver","honourhome","hrimnors_resolve","leer_cast","malachais_simula","mindspiral","rats_nest","rime_gaze","scolds_bridle","skullhead","starkonjas_head","the_bringer_of_rain","the_broken_crown","the_gull","the_peregrine","the_three_dragons","the_vertex","veil_of_the_night","ylfebans_trickery"],
			shields: ["aegis_aurora","atziris_mirror","brinerot_flag","broken_faith","chalice_of_horrors","chernobogs_pillar","crest_of_perandus","daressos_courage","demigods_beacon","great_old_ones_ward","jaws_of_agony","kaltenhalt","lioneyes_remorse","maligaros_lens","matua_tupuna","mutewind_pennant","prism_guardian","rathpith_globe","redblade_banner","rise_of_the_phoenix","saffells_frame","sentaris_answer","springleaf","the_deep_ones_hide","thousand_teeth_temu","titucius_span","trolltimber_spire","wheel_of_the_stormsail"],
			quiver: ["asphyxias_wrath","blackgleam","craghead","drillneck","hyrris_bite","maloneys_nightfall","rearguard","soul_strike"],
			axes: ["atziris_disfavour","dreadarc","dyadus","jack_the_axe","kaoms_primacy","kingmaker","limbsplit","moonbenders_wing","reapers_pursuit","relentless_fury","soul_taker","the_blood_reaper","the_harvest","the_screaming_eagle","wideswing","wings_of_entropy"],
			bows: ["chin_sol","darkscorn","deaths_harp","doomfletch","infractem","lioneyes_glare","nulls_inclination","quill_rain","silverbranch","storm_cloud","voltaxic_rift","windripper"],
			claws: ["al_dhih","bloodseeker","cybils_paw","essentia_sanguis","izaros_dilemma","last_resort","mortem_morsu","ornament_of_the_east","wildslash"],
			daggers: ["binos_kitchen_knife","bloodplay","divinarius","goredrill","heartbreaker","mark_of_the_doubting_knight","mightflay","the_consuming_dark","ungils_gauche"],
			maces: ["brightbeak","brutus_lead_sprinkler","callinellus_malleus","camerias_maul","chober_chaber","deaths_hand","doon_cuebiyari","doryanis_catalyst","flesh-eater","geofris_baptism","gorebreaker","hrimnors_hymn","jorrhasts_blacksteel","kongors_undying_rage","laviangas_wisdom","marohi_erqi","mjolner","montreguls_grasp","nyctas_lantern","quecholli","spine_of_the_first_claimant","the_dark_seer","the_supreme_truth","voidhome"],
			staves: ["agnerod_east","agnerod_north","agnerod_south","dying_breath","fencoil","hegemonys_era","pillar_of_the_caged_god","pledge_of_hands","realmshaper","sire_of_shards","taryns_shiver","the_blood_thorn","the_searing_touch","the_stormheart","the_whispering_ice","tremor_rod"],
			swords: ["aurumvorax","chitus_needle","doomsower","dreamfeather","edge_of_madness","ephemeral_edge","fidelitas_spike","hyaons_fury","ichimonji","lakishus_blade","oros_sacrifice","prismatic_eclipse","queens_decree","rebuke_of_the_vaal","redbeak","rigvalds_charge","shiversting","terminus_est","the_goddess_bound","the_goddess_scorned","the_princess"],
			wands: ["abberaths_horn","apeps_rage","lifesprig","midnight_bargain","moonsorrow","piscators_vigil","reverberation_rod","twyzel","void_battery"],
			flask: ["atziris_promise","blood_of_the_karui","divination_distillate","doedres_elixir","forbidden_taste","laviangas_spirit","lions_roar","rumis_concoction","taste_of_hate"],
			active_gems: ["abyssal_cry","anger","animate_guardian","animate_weapon","arc","arctic_armour","arctic_breath","assassins_mark","ball_lightning","barrage","bear_trap","blink_arrow","blood_rage","bone_offering","burning_arrow","clarity","cleave","cold_snap","conductivity","conversion_trap","convocation","cyclone","decoy_totem","desecrate","determination","detonate_dead","devouring_totem","discharge","discipline","dominating_blow","double_strike","dual_strike","elemental_hit","elemental_weakness","enduring_cry","enfeeble","ethereal_knives","explosive_arrow","fire_nova_mine","fire_trap","fireball","firestorm","flame_dash","flame_surge","flame_totem","flameblast","flammability","flesh_offering","flicker_strike","freeze_mine","freezing_pulse","frenzy","frost_blades","frost_wall","frostbite","glacial_cascade","glacial_hammer","grace","ground_slam","haste","hatred","heavy_strike","herald_of_ash","herald_of_ice","herald_of_thunder","ice_crash","ice_nova","ice_shot","ice_spear","immortal_call","incinerate","infernal_blow","kinetic_blast","leap_slam","lightning_arrow","lightning_strike","lightning_tendrils","lightning_trap","lightning_warp","magma_orb","mirror_arrow","molten_shell","molten_strike","phase_run","poachers_mark","poison_arrow","power_siphon","projectile_weakness","puncture","punishment","purity_of_elements","purity_of_fire","purity_of_ice","purity_of_lightning","rain_of_arrows","raise_spectre","raise_zombie","rallying_cry","reave","reckoning","rejuvenation_totem","righteous_fire","riposte","searing_bond","shield_charge","shock_nova","shockwave_totem","smoke_mine","spark","spectral_throw","split_arrow","static_strike","storm_call","summon_chaos_golem","summon_flame_golem","summon_ice_golem","summon_raging_spirit","summon_skeletons","sweep","tempest_shield","temporal_chains","tornado_shot","vengeance","vigilant_strike","viper_strike","vitality","vulnerability","warlords_mark","whirling_blades","wild_strike","wrath"],
			support_gems: ["added_chaos_damage","added_cold_damage","added_fire_damage","added_lightning_damage","additional_accuracy","blind","block_chance_reduction","blood_magic","bloodlust","cast_on_critical_strike","cast_on_death","cast_on_melee_kill","cast_when_damage_taken","cast_when_stunned","chain","chance_to_flee","chance_to_ignite","cold_penetration","cold_to_fire","concentrated_effect","culling_strike","curse_on_hit","elemental_proliferation","empower","endurance_charge_on_melee_stun","enhance","enlighten","faster_attacks","faster_casting","faster_projectiles","fire_penetration","fork","fortify","generosity","greater_multiple_projectiles","hypothermia","ice_bite","increased_area_of_effect","increased_burning_damage","increased_critical_damage","increased_critical_strikes","increased_duration","innervate","iron_grip","iron_will","item_quantity","item_rarity","knockback","less_duration","lesser_multiple_projectiles","life_gain_on_hit","life_leech","lightning_penetration","mana_leech","melee_damage_on_full_life","melee_physical_damage","melee_splash","minion_and_totem_elemental_resistance","minion_damage","minion_life","minion_speed","multiple_traps","multistrike","physical_projectile_attack_damage","physical_to_lightning","pierce","point_blank","power_charge_on_critical","ranged_attack_totem","reduced_mana","remote_mine","slower_projectiles","spell_echo","spell_totem","stun","trap","trap_and_mine_damage","weapon_elemental_damage"],
			vaal_gems: ["vaal_arc","vaal_burning_arrow","vaal_clarity","vaal_cold_snap","vaal_cyclone","vaal_detonate_dead","vaal_discipline","vaal_double_strike","vaal_fireball","vaal_flameblast","vaal_glacial_hammer","vaal_grace","vaal_ground_slam","vaal_haste","vaal_ice_nova","vaal_immortal_call","vaal_lightning_strike","vaal_lightning_trap","vaal_lightning_warp","vaal_molten_shell","vaal_power_siphon","vaal_rain_of_arrows","vaal_reave","vaal_righteous_fire","vaal_spark","vaal_spectral_throw","vaal_storm_call","vaal_summon_skeletons"],
			others: ["gegul_hi","gegul_sad"]
			
        },

        defaultUsage = !0, exports = window.emojiExtended = {
            addCompletion: function(object, cb) {
                return this.ready.then(function(addTextComplete) {
                    return addTextComplete(object, cb)
                }, cb), null
            },
            updated: !1,
            path: RELATIVE_PATH + "/plugins/nodebb-plugin-emoji-extended/images/",
            getPath: function(name) {
                return null != name ? "" + this.path + encodeURIComponent(name.toLowerCase()) + ".png" : this.path
			
            },
            list: [],
            ready: $.Deferred()
        }, $(document).ready(function() {
            return socket.emit("modules.emojiExtended", null, function(err, data) {
                var codeInListRegex, completePrefix, emojiSize, emojiTextCompleteData,
                    isBlockCodeContext, isInlineCodeContext, isSmileyContext, maxCount, minChars, style, zoom;
                return null != err ? (console.error("Error while initializing emoji-extended."),
                    console.error(err), exports.ready.reject(err)) : (defaultUsage = data.settings.fileSystemAccess, exports.list = data.list, exports.version = data.version, exports.updated = !0,
                    $(window).trigger("emoji-extended:updated", exports), maxCount = data.settings.completion.maxCount, minChars = data.settings.completion.minChars, completePrefix = data.settings.completion.prefix,
                    zoom = data.settings.zoom, emojiSize = 20, style = '<style type="text/css">\n  .emoji {\n     z-index: 0;\n  }', zoom > 0 && (zoom > 512 && (zoom = 512), style += ".emoji:hover {}\n .categories>li .content img.emoji:hover {}"), $("head").append(style + "\n</style>"), isInlineCodeContext = function(line) {


					var begin, beginSize,
                            char, currentSize, escaped, i, ignoreSince, len, since;
                        for (beginSize = 0, currentSize = 0, escaped = !1, begin = !0, since = "", ignoreSince = /^\s+`*$/, i = 0, len = line.length; len > i; i++) char = line[i],
                            "`" !== char || escaped && begin || ignoreSince.test(since) ? currentSize === beginSize && beginSize ? (beginSize = currentSize = 0, begin = !0, since = "") : (begin && beginSize && (begin = !1), since += char, currentSize = 0) : begin ? beginSize++ : (currentSize++, since += char), escaped = "\\" === char;
                        return beginSize
                    }, codeInListRegex = function(indent) {
                        return 3 === indent ? /^( {8}\s|( {0,3}\t){2}\s| {0,3}\t {4}\s| {4,7}\t\s)/ : new RegExp("^( {" + (indent + 6) + "}|( {0,3}\\t){2}| {0,3}\\t( {0," + (indent + 2) + "})| {4,7}\\t)")
                    }, isBlockCodeContext = function(lines) {
                        var code, codeInList, codeR, empty, emptyR, i, l, len, line, list, listR, prevEmpty;
                        for (list = !1, code = !1, prevEmpty = !0, emptyR = /^\s*$/, listR = /^( {0,3})[\+*-]\s/, codeR = /^ {4,}| {0,3}\t/, codeInList = null, i = 0, len = lines.length; len > i; i++) line = lines[i], empty = emptyR.test(line), list = list && !(prevEmpty && empty),
                            (l = line.match(listR)) && (list = !0, codeInList = codeInListRegex(l[1].length)), code = list && codeInList.test(line) || !list && (prevEmpty || code) && codeR.test(line), prevEmpty = empty;
                        return code
                    }, isSmileyContext = function(term) {
                        var lines;
                        return lines = term.match(/^.*$/gm), !(isInlineCodeContext(lines[lines.length - 1]) || isBlockCodeContext(lines))
                    },
                    emojiTextCompleteData = {
                        match: new RegExp("^((([\\s\\S]*)(" + completePrefix + ")):[\\w\\d+-]{" + minChars + ",})$", "i"),
                        search: function(term, callback) {
                            var regexp, smileyPrefix;
                            return isSmileyContext(term) ?
                                (smileyPrefix = term.match(/:([\w\d\+-]*)$/)[1], regexp = new RegExp("^" + smileyPrefix.replace(/\+/g, "\\+"), "i"), callback($.grep(exports.list, function(emoji) {
                                    return regexp.test(emoji)
                                }))) : void callback([])
                        },
                        replace: function(value) {
                            return "$2:" + value.toLowerCase() + ": "
                        },
                        template: function(value) {
                            return "<img class='emoji_not_responsive' src='" + exports.getPath(value) + "' />" + value
                        },
                        maxCount: maxCount,
                        index: 1
                    },
                    exports.ready.resolve.call(exports, function(object, cb) {
                        return object instanceof $ || (object = $(object)), object.data("emoji-extended-added") ? void("function" == typeof cb && cb(new Error("Already added"))) : (object.data("emoji-extended-added", "1"), object.textcomplete([emojiTextCompleteData], TEXTCOMPLETE_OPTIONS),
                            "function" == typeof cb ? cb() : void 0)
                    }))
            })
        }), 
		
		$(window).on("action:composer.loaded", function(ignored, data) {
            return exports.addCompletion($("#cmp-uuid-" + data.post_uuid + " textarea.write"))
        }),
		
		
		//$(document).on("mouseenter", ".emoji", function (evt) {					
		//	var $element = $(evt.target); //element becomes emoji?
		//	var restore = $element.data("restore-url");	//restore becomes 
		//	$element.attr("src", restore != null ? restore : "http://i.imgur.com/pdm186w.png");
		//}),			

		/*$(document).on({
			mouseenter: function(evt) {
				var $element = $(evt.target); //element becomes emoji?
				//var restore = $element.data("restore-url");	//restore becomes 
				
				var orig_src = $element.attr('src');
				
				//$element.attr("src", restore != null ? restore : "http://i.imgur.com/pdm186w.png");
				//$element.data("http://i.imgur.com/pdm186w.png", $element.attr("src"));
				$element.attr("src", "http://i.imgur.com/pdm186w.png");
			},
			mouseleave: function(evt) {
				var $element = $(evt.target);
				$element.attr("src", "http://i.imgur.com/uiEF6pf.png");
			}
		}, ".emoji"),*/
		
		
		$(document).on({
			mousemove: function(evt) {			
				var offset_ = $(this).offset();
				$(this).next('img').css({
					//top: evt.pageY - $(this).offset().top,
					//left: evt.pageX - $(this).offset().left					
					top: evt.pageY - offset_.top + 50
					
				});
			},
			mouseenter: function(evt) {
				var orig_src = $(this).attr("src");
				var desc_src = orig_src.substring(0, orig_src.length-4);
				desc_src += "_d.png";
				var desc = $('<img />', {'class': 'desc_img', src: "" + desc_src + ""});	
									
				$(this).attr('title','');
				
				$(this).after(desc);
				
				
			},
			
			click: function(evt) {
				var file_name = $(this).attr("alt");
				file_name = file_name.substring(1, file_name.length-1);

				//special letter handler
				var special_path = "";
				if (file_name == "jack_the_axe"){
					special_path = "http://pathofexile.gamepedia.com/Jack,_the_Axe";
				} else if (file_name =="montreguls_grasp") {
					special_path = "http://pathofexile.gamepedia.com/Mon'tregul's_Grasp";
				} else if (file_name =="trap") {
					special_path = "http://pathofexile.gamepedia.com/Trap_(support_gem)";
				} else if (file_name =="stun") {
					special_path ="http://pathofexile.gamepedia.com/Stun_(support_gem)";
				} else if (file_name =="point_blank") {
					special_path ="http://pathofexile.gamepedia.com/Point_Blank_(support_gem)";
				} else if (file_name =="pierce") {
					special_path ="http://pathofexile.gamepedia.com/Pierce_(support_gem)";
				} else if (file_name =="melee_damage_on_full_life") {
					special_path = "http://pathofexile.gamepedia.com/Melee_Damage_on_Full_Life";
				} else if (file_name =="mana_leech") {
					special_path ="http://pathofexile.gamepedia.com/Mana_Leech_(support_gem)";
				} else if (file_name =="life_leech") {
					special_path ="http://pathofexile.gamepedia.com/Life_Leech_(support_gem)";
				} else if (file_name =="life_gain_on_hit") {
					special_path ="http://pathofexile.gamepedia.com/Life_Gain_on_Hit_(support_gem)";
				} else if (file_name == "knockback"){
					special_path ="http://pathofexile.gamepedia.com/Knockback_(support_gem)";
				} else if (file_name == "iron_grip"){
					special_path = "http://pathofexile.gamepedia.com/Iron_Grip_(support_gem)";
				} else if (file_name == "fortify") {
					special_path = "http://pathofexile.gamepedia.com/Fortify_(support_gem)";
				} else if (file_name == "endurance_charge_on_melee_stun") {
					special_path = "http://pathofexile.gamepedia.com/Endurance_Charge_on_Melee_Stun";
				} else if (file_name == "cast_on_melee_kill") {
					special_path = "http://pathofexile.gamepedia.com/Cast_on_Melee_Kill";
				} else if (file_name == "blood_magic") {
					special_path = "http://pathofexile.gamepedia.com/Blood_Magic_(support_gem)";
				} else if (file_name == "blind") {
					special_path = "http://pathofexile.gamepedia.com/Blind_(support_gem)";
				} else if (file_name == "gegul_hi" || file_name == "gegul_sad") {
					special_path = "https://namu.wiki/w/%EA%B0%9C%EA%B5%AC%EB%A6%AC";
				}

				//check 's
				var app_name = [["atziris_foible", 5], ["daressos_salute", 6], ["demigods_presence", 6], ["marylenes_fallacy", 7], ["rashkaldors_patience", 9], ["shapers_seed", 5], ["ungils_harmony", 4], ["victarios_acuity", 7], ["volls_devotion", 3], ["demigods_bounty", 6], ["doryanis_invitation", 6], ["maligaros_restraint", 7], ["meginords_girdle", 7], ["wurms_molt", 3], ["bereks_grip", 4], ["bereks_pass", 4], ["bereks_respite", 4], ["demigods_eye", 6], ["doedres_damning", 5], ["kaoms_sign", 3], ["loris_lantern", 3], ["malachais_artifice", 7], ["mings_heart", 3], ["mokous_embrace", 4], ["ngamahus_sign", 6], ["romiras_banquet", 5], ["shavronnes_revelation", 8], ["sibyls_lament", 4], ["tasalios_sign", 6], ["thiefs_torment", 4], ["valakos_sign", 5], ["ventors_gamble", 5], ["asphyxias_wrath", 7], ["hyrris_bite", 4], ["maloneys_nightfall", 6], ["deaths_oath", 4], ["greeds_embrace", 4], ["kaoms_heart", 3], ["lioneyes_vision", 6], ["bronns_lithe", 4], ["hyrris_ire", 4], ["shavronnes_wrappings", 8], ["zahndethus_cassock", 9], ["cherrubims_maleficence", 8], ["daressos_defiance", 6], ["ambus_charge", 3], ["volls_protector", 3], ["victarios_influence", 7], ["atziris_splendour", 5], ["kaoms_roots", 3], ["atziris_step", 5], ["victarios_flight", 7], ["shavronnes_pace", 8], ["lioneyes_paws", 6], ["alberons_warpath", 6], ["gangs_momentum", 3], ["nomics_storm", 5], ["demigods_stride", 6], ["atziris_acuity", 5], ["doryanis_fist", 6], ["empires_grasp", 5], ["meginords_vise", 7], ["maligaros_virtuosity", 7], ["asenaths_gentle_touch", 6], ["doedres_tenure", 5], ["sadimas_touch", 5], ["ondars_clasp", 4], ["demigods_touch", 6], ["moonbenders_wing", 9], ["kaoms_primacy", 3], ["reapers_pursuit", 5], ["deaths_harp", 4], ["lioneyes_glare", 6], ["nulls_inclination", 3], ["cybils_paw", 4], ["izaros_dilemma", 4], ["binos_kitchen_knife", 3], ["ungils_gauche", 4], ["camerias_maul", 6], ["laviangas_wisdom", 7], ["brutus_lead_sprinkler", 5], ["deaths_hand", 4], ["doryanis_catalyst", 6], ["nyctas_lantern", 4], ["geofris_baptism", 5], ["jorrhasts_blacksteel", 7], ["kongors_undying_rage", 5], ["hyaons_fury", 4], ["lakishus_blade", 6], ["chitus_needle", 5], ["oros_sacrifice", 2], ["queens_decree", 4], ["rigvalds_charge", 6], ["hegemonys_era", 7], ["taryns_shiver", 4], ["abberaths_horn", 7], ["piscators_vigil", 7],["atziris_mirror", 5], ["chernobogs_pillar", 8], ["daressos_courage", 6], ["demigods_beacon", 6], ["great_old_ones_ward", 12], ["lioneyes_remorse", 6], ["maligaros_lens", 7], ["saffells_frame", 6], ["sentaris_answer", 6], ["titucius_span", 7], ["hrimnors_resolve", 6], ["alphas_howl", 4], ["fairgraves_tricorne", 9], ["starkonjas_head", 8], ["rats_nest", 2], ["asenaths_mark", 6], ["chitus_apex", 5], ["doedres_scorn", 5], ["scolds_bridle", 4], ["ylfebans_trickery", 6], ["devotos_devotion", 5], ["geofris_crest", 5], ["malachais_simula", 7], ["demigods_triumph", 6], ["doedres_elixir", 5], ["laviangas_spirit", 7], ["atziris_promise", 5], ["lions_roar", 3], ["rumis_concoction", 3], ["poachers_mark", 6], ["assassins_mark", 7], ["fidelitas_spike", 8], ["atziris_disfavour", 5], ["hrimnors_hymn", 6], ["apeps_rage", 3], ["the_deep_ones_hide", 11]];
				var check_app = false;
				var j = 0;
				var length = file_name.length;				
				var first_file_name;
				var second_file_name;
				
				while (j < app_name.length && check_app == false) {
					if (file_name == app_name[j][0]) {
						first_file_name = file_name.substring(0, app_name[j][1]+1);
						second_file_name = file_name.substring(app_name[j][1]+1, length);
						file_name = first_file_name + "%27" + second_file_name;
						check_app = true;
					}
					j++;
				}				
				
				
				//make first letter capital
				function cap_letter(string) {
					return string.charAt(0).toUpperCase() + string.slice(1);
				}
				//upper case first letter				
				file_name = cap_letter(file_name);
				var i = 1;

				//find new length again
				length = file_name.length;
			
				while (i < length) {
					if (file_name.charAt(i) == "_") {
						// check _of_
						if (file_name.substring(i, i+4) == "_of_") {
						// check _the_
						} else if (file_name.substring(i, i+5) == "_the_") {
						// check _and_
						} else if (file_name.substring(i, i+5) == "_and_") {
						// check _from_
						} else if (file_name.substring(i, i+6) == "_from_") {
						// check _to_
						} else if (file_name.substring(i, i+4) == "_to_") {
						// check _when_
						} else if (file_name.substring(i, i+6) == "_when_") {
						} else {
							file_name = file_name.substring(0, i+1) + cap_letter(file_name.substring(i+1, length));
						}
					}
					i++;
				}
				
				
				var wiki_path = "http://pathofexile.gamepedia.com/";
				wiki_path += file_name;
				
				//open link
				if (special_path == "") {
					window.open(wiki_path, '_blank');
				} else {
					window.open(special_path, '_blank');
				}
			},
			
			mouseleave: function(evt) {
				$('.desc_img').remove();
			}
		}, ".emoji"),		
		
        $(window).on("action:chat.loaded", function(ignored, modal) {
            return exports.addCompletion($("#chat-message-input", modal))
        }), $(window).trigger("emoji-extended:initialized", exports),
        exports.ready.then(function() {
            var lists;
            return lists = defaultUsage ? defaultLists : {
                smileys: exports.list
            }, require(["composer/formatting", "composer/controls"], function(formatting, controls) {
                return formatting.addButtonDispatch("emoji-extended", function(area, sS, sE) {
                    var activeLink, currentTabItem, dialog, getLink, getTabItem, items, key, modalContent, modalTabContent, modalTabPanel, modalTabs, setActive;
                    activeLink = null, getLink = function(value) {
                            var link;
                            return link = $("<a class='emoji-link'></a>"), link.html("<img class='emoji_not_responsive' src='" + exports.getPath(value) + "' />&nbsp;:" + value + ":"),
                                link.click(function() {
                                    return dialog.modal("hide"), controls.updateTextareaSelection(area, sE, sE), controls.insertIntoTextarea(area, ":" + value + ": "),
                                        controls.updateTextareaSelection(area, sS === sE ? sE + 3 + value.length : sS, sE + 3 + value.length)
                                })
                        }, getTabItem = function(value, items) {
                            var firstUpper, li, link;
                            return firstUpper = value[0].toUpperCase() + value.substring(1), link = $('<a role="tab">' + firstUpper + "</a>"), li = $("<li class='emoji-tab'></li>").append(link),
                                link.click(function() {
                                    return setActive(li, items)
                                }), li
                        }, setActive = function(li, items) {
                            var i, item, len, results;
                            if (li !== activeLink && (null != activeLink && "function" == typeof activeLink.removeClass && activeLink.removeClass("active"),
                                    activeLink = null != li && "function" == typeof li.addClass ? li.addClass("active") : void 0, modalTabPanel.html(""), null != items)) {
                                for (results = [], i = 0, len = items.length; len > i; i++) item = items[i], results.push(modalTabPanel.append(getLink(item)));
                                return results
                            }
                        }, modalContent = $('<div role="tabpanel"></div>'), modalTabs = $('<ul class="nav nav-tabs" role="tablist"></ul>'), modalTabContent = $('<div class="tab-content"></div>'),
                        modalTabPanel = $('<div role="tabpanel" class="tab-pane active"></div>').appendTo(modalTabContent);
                    for (key in lists) items = lists[key], currentTabItem = getTabItem(key, items), modalTabs.append(currentTabItem),
                        null == activeLink && setActive(currentTabItem, items);
                    return modalContent.append(modalTabs), modalContent.append(modalTabContent), dialog = bootbox.dialog({
                        title: "Eye of Leopard ",
                        message: modalContent,
                        onEscape: function() {}
                    }), dialog.addClass("emoji-dialog")
                })
            })
        })
}();